<!--Появление кнопки вверх при прокрутке страницы-->
    $(function() {
    $(window).scroll(function() {
        if($(this).scrollTop() > 300) {
            $('#toTop').fadeIn();
            $('#toTop .glyphicon').fadeIn();
        } else {
        $('#toTop').fadeOut();
        $('#toTop .glyphicon').fadeOut();
        }
    });
        $('#toTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });
});