'use strict';

Vue.config.productionTip = true;
Vue.config.devtools = true;

Vue.use(VeeValidate);

Vue.component('feedback', {
    data() {
        return {
            title: 'Write to us',
            send_btn_text: 'Send a question',
            sending: false,
            feedback: {
                name: null,
                city: null,
                phone: null,
                email: null,
                question: null,
                image: null
            }
        }
    },
    template: `<div class="feedback">
                <form class="feedback-form" enctype="multipart/form-data" @submit.prevent>
                    <div class="feedback-form__title">{{ title }}</div>

                    <div class="feedback-form-group">
                        <input type="text" v-model="feedback.name" name="name" v-validate="'required|alpha_spaces'" placeholder="Name" :class="{'is-danger': errors.has('name'), 'feedback-form__input': true}" :disabled="sending">
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="feedback-form-group">
                                <input type="text" v-model="feedback.city" name="city" v-validate="'required|alpha_spaces'" placeholder="City" :class="{'is-danger': errors.has('city'), 'feedback-form__input': true}" :disabled="sending">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="feedback-form-group">
                                <input type="text" v-model="feedback.phone" name="phone" v-validate="'required|numeric'" placeholder="Phone" :class="{'is-danger': errors.has('phone'), 'feedback-form__input': true}" :disabled="sending">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="feedback-form-group">
                                <input type="text" v-model="feedback.email" name="email" v-validate="'required|email'" placeholder="Email" :class="{'is-danger': errors.has('email'), 'feedback-form__input': true}" :disabled="sending">
                            </div>
                        </div>
                    </div>

                    <div class="feedback-form-group">
                        <textarea type="text" v-model="feedback.question" name="question" v-validate="'required'" placeholder="Question" :class="{'is-danger': errors.has('question'), 'feedback-form__input': true, 'feedback-form__input_textarea': true}" :disabled="sending"></textarea>
                    </div>
                    
                    <div class="feedback-form-group">
                        <input type="file" name="image" v-validate="'mimes:image/jpeg|ext:jpg'" @change="onFile($event)" :disabled="sending">
                    </div>

                    <div class="feedback-form-group">
                        <a @click="submit()" :class="{'feedback-form__btn': true, 'feedback-form__btn_sending': sending}">{{ send_btn_text }}</a>
                    </div>
                </form>
               </div>`,
    methods: {
        submit: function () {
            if (this.sending) {
                return false;
            }

            this.$validator.validateAll().then(result => {
                let self = this;

                if (result) {
                    let formData = new FormData();

                    for (let key in this.feedback) {
                        if (key === 'image') {
                            continue;
                        }

                        formData.append(key, this.feedback[key]);
                    }

                    if (this.feedback.image) {
                        formData.append('image', this.feedback.image, this.feedback.image.name);
                    }

                    this.sending = true;
                    this.send_btn_text = 'Sending...';

                    this.$http.post('./sendmail.php', formData).then(response => {
                        self.reset();

                        this.send_btn_text = 'Send a question';
                        this.sending = false;
                    }, error => {
                       console.log(error);
                    });
                }
            });
        },
        reset: function () {
            const clear = async () => {
                this.feedback.name = null;
                this.feedback.email = null;
                this.feedback.city = null;
                this.feedback.phone = null;
                this.feedback.question = null;
                this.feedback.image = null;
            };

            clear().then(() => {
                this.$validator.reset()
            })
        },
        onFile(event) {
            this.feedback.image = event.target.files[0];
        }
    }
});

new Vue({
    el: '#app',
});